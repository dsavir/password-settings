# Password Settings

Code that demonstrates how to password protect one (or more) specific setting in Android, using PreferenceFragmentCompat.   

See the Medium post here: https://dsavir-h.medium.com/password-protected-setting-in-android-3e0f120a1c3f
